#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"library.h"
#include "date.h"


void labrarien_area(user_t *u) {
	int choice;
	char name[80];
	do {
		printf("\n\n0. Sign Out\n1. Add member\n2. Edit Profile\n3. Change Password\n4. Add Book\n5. Find Book\n6. Edit Book\n7. Check Availability\n8. Add Copy\n9. Change Rack\n10. Issue Copy\n11. Return Copy\n12. Take Payment\n13. Payment History\nEnter choice: ");
		scanf("%d", &choice);
		switch(choice) {
			case 1: // Add member
				add_member();
				break;

			case 2:
			   edit_profile(u);
			   
				break;
			case 3:
			 change_password(u);

				break;
			case 4: // Add Book
				add_book();
				break;
			case 5: // Find Book
				printf("Enter book name: ");
				scanf("%s", name);
				book_find_by_name(name);
				break;
			case 6: // Edit Book
				book_edit_by_id();
				break;
			case 7:
			    bookcopy_checkavail_details();
				break;
			case 8:
			     bookcopy_add();
				break;
			case 9:

				break;
			case 10:
			bookcopy_issue();

				break;
			case 11:
			bookcopy_return();

				break;
			case 12:
				break;
			case 13:
				break;
		}
	}while (choice != 0);		
}

void add_member() {
	// input member details
	user_t u;
	user_accept(&u);
	// add librarian into the users file
	user_add(&u);
}

void add_book() {
	FILE *fp;
	// input book details
	book_t b;
	book_accept(&b);
	b.id = get_next_book_id();
	// add book into the books file
	// open books file.
	fp = fopen(BOOK_DB, "ab");
	if(fp == NULL) {
		perror("cannot open books file");
		exit(1);
	}
	// append book to the file.
	fwrite(&b, sizeof(book_t), 1, fp);
	printf("book added in file.\n");
	// close books file.
	fclose(fp);
}

void book_edit_by_id() 
{
	int id, found = 0;
	FILE *fp;
	book_t b;
	// input book id from user.
	printf("enter book id: ");
	scanf("%d", &id);
	// open books file
	fp = fopen(BOOK_DB, "rb+");
	if(fp == NULL) {
		perror("cannot open books file");
		exit(1);
	}
	// read books one by one and check if book with given id is found.
	while(fread(&b, sizeof(book_t), 1, fp) > 0) {
		if(id == b.id) {
			found = 1;
			break;
		}
	}
	// if found
	if(found) {
		// input new book details from user
		long size = sizeof(book_t);
		book_t nb;
		book_accept(&nb);
		nb.id = b.id;
		// take file position one record behind.
		fseek(fp, -size, SEEK_CUR);
		// overwrite book details into the file
		fwrite(&nb, size, 1, fp);
		printf("book updated.\n");
	}
	else // if not found
		// show message to user that book not found.
		printf("Book not found.\n");
	// close books file
	fclose(fp);
}
void bookcopy_add()
{
	FILE *fp;
	bookcopy_t b;
    bookcopy_accept(&b);
	b.id = get_next_book_id();
	fp = fopen(BOOKCOPY_DB, "ab");
	if (fp = NULL);		
	{
		perror("canot open book copies");
		exit(1);
	}
	fwrite(&b, sizeof(bookcopy_t),1, fp);
	printf("book copy added in file. \n");
	fclose(fp);

}
void bookcopy_checkavail_details()
{
	int book_id;
	FILE *fp;
	bookcopy_t bc;
	int count = 0;
	printf("Enter the book id: ");
	scanf("%d", &book_id);
	fp = fopen(BOOKCOPY_DB, "rb");
	if (fp = NULL)
	{
		perror("connot open bookcopies file : ");
		return;	
	}
	while (fread(&bc, sizeof(bookcopy_t),1,fp) > 0)
	{
		if (bc.bookid == book_id && strcmp(bc.status, STATUS_AVAIL) == 0)
		{
			bookcopy_display(&bc);
			count++;
		}
	}
	fclose(fp);
	if (count == 0)
	{
		printf("no copies availables : ");

	}
}
void bookcopy_issue()
{
	issuerecord_t rec;
	FILE *fp;
	issuerecord_accept(&rec);
	rec.id=get_next_issuerecord_id();
	fp = fopen(ISSUERECORD_DB, "ab");
	if (fp == NULL)
	{
		perror("issuerecord file cannot opened");
		exit(1);
	}
	fwrite(&rec, sizeof(issuerecord_t),1, fp);
	fclose(fp);
	bookcopy_changestatus(rec.copyid, STATUS_ISSUED);
}

void bookcopy_changestatus(int bookcopy_id, char status[])
{
	bookcopy_t bc;
	FILE *fp;
	long size = sizeof(bookcopy_t);
	fp = fopen(BOOKCOPY_DB, "rb+");
	if (fp == NULL)
	{
		perror("cannot open book copies file");
		return;
	}
	while (fread(&bc, sizeof (bookcopy_t),1, fp) > 0)
	
	{
	   if (bookcopy_id == bc.id)
	   {
		   strcpy(bc.status, status);
		   fseek (fp, -size , SEEK_CUR);
		   fwrite(&bc, sizeof(bookcopy_t), 1, fp);
		   break;

	   }
	   
	}
	
	fclose(fp);
}
void display_issued_bookcopies(int member_id)
{
	FILE *fp;
	issuerecord_t rec;
	fp = fopen(ISSUERECORD_DB,"rb");
	if (fp == NULL)
	{
		perror("cannot open issue record file");
		return;
	}
	while (fread (&rec, sizeof(issuerecord_t),1, fp) > 0)
	{
	if (rec.memberid == member_id && rec.return_date.day == 0)
	{
		issuerecord_display(&rec);
	}
		fclose(fp);
	}

}
void bookcopy_return()
{
	int member_id, record_id;
	FILE *fp;
	issuerecord_t rec;
	int diff_days, found = 0;
	long size = sizeof(issuerecord_t);
	printf("enter the id : ");
	scanf("%d",&member_id);
	display_issued_bookcopies(member_id);
	printf("enter the issues record id (to return)");
	scanf("%d",&record_id);
	fp == fopen(ISSUERECORD_DB,"rb+");
	if (fp==NULL)
	{
		perror("cannot open issue file");
		return;

	}
	while (fread(&rec, sizeof(issuerecord_t), 1, fp) > 0)
	{
		if(record_id == rec.id) 
		{
			found = 1;
			// initialize return date
			rec.return_date = date_current();
			// check for the fine amount
			diff_days = date_cmp(rec.return_date, rec.return_duedate);
			// update fine amount if any
			if(diff_days > 0)
			{
				rec.fine_amount = diff_days * FINE_PER_DAY;
			  break;
	        }
	   }
	    if(found) 
		{
		
		fseek(fp, -size, SEEK_CUR);
	
		fwrite(&rec, sizeof(issuerecord_t), 1, fp);
	
		printf("issue record updated after returning book:\n");
		issuerecord_display(&rec);
	
		bookcopy_changestatus(rec.copyid, STATUS_AVAIL);
	    }
	
	}

	fclose(fp);
	
}
 void edit_profile(user_t *u)
{

    int found = 0;
	FILE *fp;
	user_t b;
	// input user id from user.
	//printf("enter user id: ");
	//scanf("%d", &id);
	// open user file
	 
	fp = fopen(USER_DB, "rb+");
	if(fp == NULL) {
		perror("cannot open user file");
		exit(1);
	}
	// read books one by one and check if book with given id is found.
	while(fread(&b, sizeof(user_t), 1, fp) > 0) {
		if( b.id == u->id)
		 {
			found = 1;
			break;
		 }
	}
	// if found
	if(found) {
		
		// input new user details from user
		long size = sizeof(user_t);
		user_t nb;
		//user_accept(&nb);
		nb.id = b.id;
		strcpy(nb.name,u->name);
		strcpy(nb.password,u->password);
		strcpy(nb.role,u->role);
		printf("\nEnter the new Email id :");
		scanf("%s",nb.email);
		printf("\nEnter the new phone number :");
		scanf("%s",nb.phone);
		// take file position one record behind.
		fseek(fp, -size, SEEK_CUR);
		// overwrite book details into the file
		fwrite(&nb, size, 1, fp);
		printf("user updated.\n");
	}
	else // if not found
		// show message to user  not found.
		printf("user not found.\n");
	// close books file
	fclose(fp);
}

void change_password(user_t *u)
{
	 int found = 0;
	FILE *fp;
	user_t b;
	// input user id from user.
	//printf("enter user id: ");
	//scanf("%d", &id);
	// open user file
	 
	fp = fopen(USER_DB, "rb+");
	if(fp == NULL) {
		perror("cannot open user file");
		exit(1);
	}
	// read books one by one and check if book with given id is found.
	while(fread(&b, sizeof(user_t), 1, fp) > 0) {
		if( b.id == u->id)
		 {
			found = 1;
			break;
		 }
	}
	// if found
	if(found) {
		
		// input new user details from user
		long size = sizeof(user_t);
		user_t nb;
		//user_accept(&nb);
		nb.id = b.id;
		strcpy(nb.name,u->name);
		strcpy(nb.email,u->email);
		strcpy(nb.role,u->role);
		printf("\nEnter the current password :");
		scanf("%s",b.password);
		printf("\nEnter the new password :");
		scanf("%s",nb.password);
		printf("\nEnter the agine new password :");
		scanf("%s",nb.password);
		
		// take file position one record behind.
		fseek(fp, -size, SEEK_CUR);
		// overwrite book details into the file
		fwrite(&nb, size, 1, fp);
		printf("user updated.\n");
	}
	else // if not found
		// show message to user  not found.
		printf("user not found.\n");
	// close books file
	fclose(fp);

}