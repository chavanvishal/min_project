#ifndef _LIBRARY_H
#define _LIBRARY_H

#include "date.h"

#define    USER_DB             "user.db"
#define    BOOK_DB             "book.db"
#define    ROLE_OWNER           "owner"
#define    ROLE_LIBRARIAN      "librarian"
#define    ROLE_MEMBER         "member"
#define    STATUS_AVAIL         "avalable"
#define    STATUS_ISSUED       "issued"
#define    PAY_TYPE_FEES        "fee"
#define    PAY_TYPE_FINE         "fine"
#define    FINE_PER_DAY              5
#define    BOOK_RETURN_DAYS          7
#define    MEMBERSHIP_MONTH_DAYS     30
#define    EMAIL_OWNER       "chavanvishu777@gmail.com"
#define    ISSUERECORD_DB "issuerecord.db"
#define    BOOKCOPY_DB    "bookcopies.db"


 typedef struct user {

    int id;
    char name[30];
    char email[30];
    char phone[15];
    char password[10];
    char role[15];

}user_t;

typedef struct book{

    int id;
    char name[30];
    char auther[30];
    char subject[15];
    double price;
    char isbn[15];

}book_t;

typedef struct bookcopy{
    
    int id;
    int bookid;
    int rack;
    char status[16];
    
}bookcopy_t;


typedef struct issuerecord{

    int id;
    int copyid;
    int memberid;
    date_t issue_date;
    date_t return_duedate;
    date_t return_date;
    double fine_amount;

}issuerecord_t;


typedef struct pyment{
    int id;
    int memberid;
    double amount;
    char type[10];
    date_t tx_time;
    date_t next_pay_duedate;

}payment_t;


//book function 
void book_edit_by_id();
void bookcopy_add();
void bookcopy_checkavail_details();
void bookcopy_changestatus(int bookcopy_id, char status[]);
void bookcopy_return();
void bookcopy_display(bookcopy_t *c);

// user function
void user_accept(user_t *u);
void user_display(user_t *u);


// book function;
void book_accept(book_t *b);
void book_display(book_t *b);
void bookcopy_issue();
void add_book();
int get_next_book_id();
void book_find_by_name(char name[]);
void bookcopy_accept(bookcopy_t *c);
//int get_next_issuerecord_id();


// owner function
void owner_area(user_t *u);
void appoint_librarian();

//member function 
void member_area(user_t *u);
void bookcopy_checkavail();
void bookcopy_checkavail_details();


//labrarien function
void labrarien_area(user_t *u);
int get_next_user_id();
void add_member();
//comman function

void sign_in();
void sign_up();

void edit_profile(user_t *u);
void change_password(user_t *u);

void user_add(user_t *u);
int user_find_by_email(user_t *u, char email[]);

void book_find_by_name_( char name[]);
 //issuercord function
void issuerecord_accept(issuerecord_t *r);
void issuerecord_display(issuerecord_t *r);

// chnange password 
int get_next_user_id();
int get_next_book_id();
int get_next_bookcopy_id();
int get_next_issuerecord_id();

#endif

